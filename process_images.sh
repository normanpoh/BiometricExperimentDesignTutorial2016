############################################################
# You run this Bash script from within the virtual machine (VM) provided.
# The user name is Surrey; and the password is Surrey2016
#
# You need to first configure the VM to enable sharing of C drive
############################################################

############################################################
# change the directory to where the files are:
############################################################
cd /mnt/hgfs/C/Users/user/Documents/GitHub/Biometrics/FingerprintVulnerabilityWorkshop2016/tutorial

############################################################
# convert png files to wsq files
############################################################
for i in *.png
do
convert $i `basename $i .png`.wsq
done

############################################################
# measure the fingerprint quality
############################################################
for i in *.wsq
do
quality=`nfiq $i` 
echo $i $quality >> quality.txt
done

############################################################
# extract the features
############################################################
for i in *.wsq
do
mindtct $i features/$i
done


############################################################
# Perform the matching
############################################################
# create a list of template
find features -name \*.xyt > xyt.list


for m in `cat xyt.list`
do
  fname=`basename $m .xyt`
  fname="scores/$fname.scores"
  bozorth3 -o $fname -p $m -G xyt.list 
done

############################################################
# Launch matlab and work there
############################################################

# launch matlab in your host environment
# change the directory to mScripts_analyse_scores
# edit the script main_process_scores.m