fname = dir('../scores/*.scores');
fname = {fname(:).name};
%% load the scores
for i=1:numel(fname),
  scores(:,i) = load(['../scores/' fname{i}]);
end;

imagesc(scores)
%% set the diagonal score to zero 
% we do this because the diagonal scores are comparison with the same image
mask = 1- eye(numel(fname));

scores1 = scores .* mask;

imagesc(scores1)

%% get symmetric scores
% we use only symmetric scores because fingerprint matcher is not
% symmetric, i.e., compare sample A with B is not the same as comparing B
% with A
scores_sym = 0.5 * (triu(scores1) + tril(scores1)');

imagesc(scores_sym)
%% get the mask of the genuine scores

idlist = repmat([1 2 3],5,1);
idlist = idlist(:)';

mask_gen=zeros(size(scores));
for id=unique(idlist),
  selected = idlist==id;
  mask_gen(selected,selected)=1;
end;

mask_gen = mask_gen - eye(numel(fname));
mask_gen = triu(mask_gen);
imagesc(mask_gen);

%% prepare the impostor score mask
mask_imp = ones(size(mask_gen)) - mask_gen - eye(numel(fname));
mask_imp = triu(mask_imp);
imagesc(mask_imp)

%% get the genuine scores
imagesc(mask_gen .* scores_sym)

%% get the impostor scores mask
imagesc(mask_imp .* scores_sym)

%% get the scores
imp_scores = scores_sym(mask_imp==1);
gen_scores = scores_sym(mask_gen==1);

%% plot the error rate curve
addpath ../wer/lib/deliverable_DETconf/lib
[eer, thrd] = wer(imp_scores, gen_scores,[],1);