# This is the actual script that I used to process LivDET data

############################################################
# Extract the minutiae template
############################################################

# 2011
c=0
tot=`wc -l 2011.list`
for m in `cat 2011.list`
do
  ((c++))
  echo "Processing file no $c of $tot: $m"
  n=`echo $m | sed 's/\.\.//g;s/data\///g;s/2011//g;s/\/\///g'`
  fname=`echo $n | sed 's/\//_/g'`
  fname=`basename $fname .png`
  convert $m out.jpg
  mindtct out.jpg ../features/2011/$fname
done

# 2013
c=0
tot=`wc -l 2013.list`
for m in `cat 2013.list`
do
  ((c++))
  echo "Processing file no $c of $tot: $m"
  n=`echo $m | sed 's/\.\.//g;s/data\///g;s/2013//g;s/\/\///g'`
  fname=`echo $n | sed 's/\//_/g'`
  fname=`basename $fname .png`
  convert $m out.jpg
  mindtct out.jpg ../features/2013/$fname
done

############################################################
# NFIQ
############################################################
c=0
tot=`wc -l 2011.list`
for m in `cat 2011.list`
do
  ((c++))
  echo "Processing file no $c of $tot: $m"
  n=`echo $m | sed 's/\.\.//g;s/data\///g;s/2011//g;s/\/\///g'`
  fname=`echo $n | sed 's/\//_/g'`
  fname=`basename $fname .png`
  convert $m out.wsq
  msg="`nfiq  -v out.wsq`"
  echo $fname $msg >> 2011_nfiq.txt
done

c=0
tot=`wc -l 2013.list`
for m in `cat 2013.list`
do
  ((c++))
  echo "Processing file no $c of $tot: $m"
  n=`echo $m | sed 's/\.\.//g;s/data\///g;s/2013//g;s/\/\///g'`
  fname=`echo $n | sed 's/\//_/g'`
  fname=`basename $fname .png`
  convert $m out.wsq
  msg="`nfiq  -v out.wsq`"
  echo $fname $msg >> 2013_nfiq.txt
done

############################################################
# Perform the matching
############################################################
find ../features/2011 -name \*.xyt > 2011_xyt.list

find ../features/2013 -name \*.xyt > 2013_xyt.list

time bozorth3 -o tmp.scores -p ../features/2011/TestingBiometrikaLive_100_1.xyt -G 2011_xyt.list 

tot=`wc -l 2011_xyt.list`
c=0
for m in `cat 2011_xyt.list`
  do
  ((c++))
  fname=`basename $m .xyt`
  fname="result2011/$fname.scores"
  if [ -f $fname ]; then
      echo "File $fname exists"
  else
      echo "Processing file no $c: $fname"
      bozorth3 -o $fname -p $m -G 2011_xyt.list 
  fi
done

tot=`wc -l 2013_xyt.list`
c=0
for m in `cat 2013_xyt.list`
  do
  ((c++))
  fname=`basename $m .xyt`
  fname="result2013/$fname.scores"
  if [ -f $fname ]; then
      echo "File $fname exists"
  else
      echo "Processing file no $c: $fname"
      bozorth3 -o $fname -p $m -G 2013_xyt.list 
  fi
done
